<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>Build your Character!</title>
    <style>
        a {
            text-decoration: none;
            width: 200px;
            height: 42px;
            border-radius: 22px;
            background-color: #323232;
            display: inline-flex;
            justify-content: center;
            align-items: center;
            color: #fff !important;
        }
    </style>
</head>
<body>
<h1><%= "Character Creator" %>
</h1>
<br/>
<a href="create-a-character">Create a Character</a>
<a href="characters-list">View Characters</a>
<a href="go-to-404-plx">Go to a 404 page</a>
</body>
</html>