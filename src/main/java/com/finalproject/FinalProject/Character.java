package com.finalproject.FinalProject;

import javax.persistence.*;

@Entity
@Table(name = "`Character`", schema = "Final", catalog = "")
public class Character {
    private int id;
    private String name;
    private String race;
    private String weapon;
    private String birthsign;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "race")
    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    @Basic
    @Column(name = "weapon")
    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }

    @Basic
    @Column(name = "birthsign")
    public String getBirthsign() {
        return birthsign;
    }

    public void setBirthsign(String birthsign) {
        this.birthsign = birthsign;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Character character = (Character) o;

        if (id != character.id) return false;
        if (name != null ? !name.equals(character.name) : character.name != null) return false;
        if (race != null ? !race.equals(character.race) : character.race != null) return false;
        if (weapon != null ? !weapon.equals(character.weapon) : character.weapon != null) return false;
        if (birthsign != null ? !birthsign.equals(character.birthsign) : character.birthsign != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (race != null ? race.hashCode() : 0);
        result = 31 * result + (weapon != null ? weapon.hashCode() : 0);
        result = 31 * result + (birthsign != null ? birthsign.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "\nName: " + name +
                "\nRace: " + race +
                "\nWeapon: " + weapon +
                "\nBirthsign: " + birthsign +
                "\nId: " + id;
    }

    public String toHTML() {
        return "\n<p>Name: <span class='detail'>" + name + "</span></p>" +
                "\n<p>Race: <span class='detail'>" + race + "</span></p>" +
                "\n<p>Weapon: <span class='detail'>" + weapon + "</span></p>" +
                "\n<p>Birthsign: <span class='detail'>" + birthsign + "</span></p>" +
                "\n<p>Id: <span class='detail'>" + id + "</span></p>";
    }
}
