package com.finalproject.FinalProject;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "createACharacter", value = "/create-a-character")
public class CreateACharacterForm extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html>" +
                "<head>" +
                "<style>" +
                "* { font-size: 18px; }" +
                "h1 { font-size: 32px; }" +
                "form { width: 100%; max-width: 300px; }" +
                "a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;\n" +
                "display: inline-flex;\n" +
                "justify-content: center;\n" +
                "align-items: center; color: #fff !important; }" +
                "button { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #fff; border: 1px solid #323232;\n" +
                "display: inline-flex;\n" +
                "justify-content: center;\n" +
                "align-items: center; color: #323232 !important; }" +
                "label { display: block; }" +
                "input, select { margin: 12px 0 20px 0; width: 100%; }" +
                "</style>" +
                "</head>" +
                "<body>");
        out.println("<h1>Create your Character</h1>");
        out.println("<form action='CreateCharacter' method='post'>");
        // Name
        out.println("<br/><label for='name'>Name: </label><input type='text' name='name' id='name'/>");
        // Race
        out.println("<br/><label for='race'>Race: </label>" +
                "  <select name='race' id='race'>" +
                "  <option value='Human'>Human</option>" +
                "  <option value='Wood Elf'>Wood Elf</option>" +
                "  <option value='Dark Elf'>Dark Elf</option>" +
                "  <option value='Dwarf'>Dwarf</option>" +
                "  <option value='Orc'>Orc</option>" +
                "</select>");
        // Weapon
        out.println("<br/><label for='weapon'>Starting weapon: </label>" +
                "  <select name='weapon' id='weapon'>" +
                "  <option value='Hand to Hand'>Hand to Hand</option>" +
                "  <option value='Dagger'>Dagger</option>" +
                "  <option value='Shortsword'>Shortsword</option>" +
                "  <option value='Broadsword'>Broadsword</option>" +
                "  <option value='Axe'>Axe</option>" +
                "  <option value='Bow'>Bow</option>" +
                "  <option value='Longbow'>Longbow</option>" +
                "</select>");
        // Birth Sign
        out.println("<br/><label for='birthsign'>Birthsign: </label>" +
                "  <select name='birthsign' id='birthsign'>" +
                "  <option value='Apprentice'>Apprentice</option>" +
                "  <option value='Lady'>Lady</option>" +
                "  <option value='Mage'>Mage</option>" +
                "  <option value='Serpent'>Serpent</option>" +
                "  <option value='Shadow'>Shadow</option>" +
                "  <option value='Steed'>Steed</option>" +
                "  <option value='Thief'>Thief</option>" +
                "  <option value='Warrior'>Warrior</option>" +
                "</select>");
        // Submit
        out.println("<button type='submit'>Save</button>");
        out.println("</form>");
        out.println("<a href=\"characters-list\">Character List</a>\n" +
                "<a href=\"\\FinalProject_war_exploded\">Back Home</a>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}