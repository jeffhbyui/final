package com.finalproject.FinalProject;

import javax.persistence.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.*;

@WebServlet(name = "CharactersList", value = "/characters-list")
public class CharactersList extends HttpServlet {

    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        // Get total count
        BigInteger charsLength = (BigInteger) entityManager.createNativeQuery("SELECT count(*) FROM Final.`Character`").getSingleResult();
        String _length = charsLength.toString();

        // Iterate over list of characters
        List<Character> characterList = entityManager.createQuery("from Character", Character.class).getResultList();
        int len = characterList.size();
        System.out.println(len);
        Iterator<Character> it = characterList.iterator();
        out.println("<html>" +
                "<head>" +
                "<style>" +
                "* { font-size: 18px; }" +
                "h1 { font-size: 32px; }" +
                "form { width: 100%; max-width: 300px; }" +
                "label { display: block; }" +
                "input, select { margin: 12px 0 20px 0; width: 100%; } " +
                "a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;\n" +
                "display: inline-flex;\n" +
                "justify-content: center;\n" +
                "align-items: center; color: #fff !important; }" +
                ".count { margin-left: 16px; }" +
                ".character-container { display: flex; flex-direction: row; flex-wrap: wrap; }" +
                ".character { max-width: 100%; width: 300px; display: flex; flex-direction: column; background-color: #ededed; padding: 16px; margin: 16px; border-radius: 8px;\n" +
                "box-shadow: 1px 2px 5px #767676;} " +
                ".character p { color: #767676; } " +
                ".character p .detail { color: #111; } " +
                "</style>" +
                "</head>" +
                "<body><p class='count'>Total Characters: <b>&nbsp;" + _length + "</b></p>");

        // open character container
        out.println("<div class='character-container'>");
        int i = 0;
        do {
            Character character = it.next();
            out.println("<div class='character'>" + character.toHTML() +
                    "<a href=\"./delete-character?id=" + character.getId() + "\">Delete</a>" +
                    "</div>");
            i++;
        } while (i < len);
        // close character container
        out.println("</div>");
        out.println("<a href=\"create-a-character\">Create a Character</a>\n" +
                "<a href=\"\\FinalProject_war_exploded\">Back Home</a>");

        transaction.commit();
        out.println("</body></html>");
    }

    public void destroy() {
    }
}
