package com.finalproject.FinalProject;

import org.junit.Assert;
import org.junit.Test;

public class Tests {
    @Test
    public void testCharacterCreation() throws Exception {
        // Pass
        ValidateCharacter vc = new ValidateCharacter();
        String name = "Name";
        String race = "Race";
        String weapon = "Weapon";
        String birthsign = "Birthsign";
        Boolean result = vc.validate(name, race, weapon, birthsign);
        Assert.assertTrue(result);

        // Failures
        String badName = "";
        Boolean nameFail = vc.validate(badName, race, weapon, birthsign);
        Assert.assertFalse(nameFail);

        String badRace = "";
        Boolean raceFail = vc.validate(name, badRace, weapon, birthsign);
        Assert.assertFalse(raceFail);

        String badWeapon = "";
        Boolean weaponFail = vc.validate(name, race, badWeapon, birthsign);
        Assert.assertFalse(weaponFail);

        String badBirthsign = "";
        Boolean birthsignFail = vc.validate(name, race, badWeapon, badBirthsign);
        Assert.assertFalse(birthsignFail);
    }
}
