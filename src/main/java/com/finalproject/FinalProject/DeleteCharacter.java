package com.finalproject.FinalProject;

import org.hibernate.JDBCException;

import java.io.*;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "deleteCharacter", value = "/delete-character")
public class DeleteCharacter extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(@org.jetbrains.annotations.NotNull HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JDBCException {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try {
            String id = request.getParameter("id").toString();
            transaction.begin();
            entityManager.remove((Character) entityManager.find(Character.class, Integer.parseInt(id)));
            // Commit to DB
            transaction.commit();

            out.println("<html>" +
                    "<head>" +
                    "<style>" +
                    "* { font-size: 18px; }" +
                    "h1 { font-size: 32px; }" +
                    "a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;\n" +
                    "display: inline-flex;\n" +
                    "justify-content: center;\n" +
                    "align-items: center; color: #fff !important; }" +
                    "form { width: 100%; max-width: 300px; }" +
                    "label { display: block; }" +
                    "input, select { margin: 12px 0 20px 0; width: 100%; }" +
                    "</style>" +
                    "</head>" +
                    "<body>");
            out.println("<h1>Character Deletion</h1>");
            out.println("<p>Character Deleted</p>");
            out.println("<a href=\"create-a-character\">Create a Character</a>\n" +
                    "<a href=\"characters-list\">View Characters</a> " +
                    "<a href=\"\\FinalProject_war_exploded\">Back Home</a>");
            out.println("</body></html>");
        } catch (JDBCException e) {
            out.println("<html><head><style>a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;" +
                    "display: inline-flex;" +
                    "justify-content: center; " +
                    "align-items: center; color: #fff !important; }</style>" +
                    "</head><body><p>Something went wrong, try to delete your character again. Be sure to fill the entire form</p><a href=\"create-a-character\">Create Character</a></body></html>");
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }

    public void destroy() {
    }
}