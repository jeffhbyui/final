package com.finalproject.FinalProject;

import org.hibernate.JDBCException;

import java.io.*;
import java.math.BigInteger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "createCharacter", value = "/CreateCharacter")
public class CreateCharacter extends HttpServlet {
    private String message;

    public void init() {
        message = "Hello World!";
    }

    public void doPost(@org.jetbrains.annotations.NotNull HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, JDBCException {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        try {
            String name = request.getParameter("name");
            String race = request.getParameter("race");
            String weapon = request.getParameter("weapon");
            String birthsign = request.getParameter("birthsign");
            ValidateCharacter validateCharacter = new ValidateCharacter();
            Boolean isValidCharacter = validateCharacter.validate(name, race, weapon, birthsign);
            if (isValidCharacter) {
                transaction.begin();
                // Get total count
                BigInteger charsLength = (BigInteger) entityManager.createNativeQuery("SELECT count(*) FROM Final.`Character`").getSingleResult();
                String _length = charsLength.toString();

                // Get next ID
                int id = Integer.parseInt(_length);
                int nextId = id + 1;

                // Build Character
                Character character = new Character();
                character.setId(nextId);
                character.setName(name);
                character.setRace(race);
                character.setWeapon(weapon);
                character.setBirthsign(birthsign);
                // Persist
                entityManager.persist(character);
                // Commit to DB
                transaction.commit();

                out.println("<html>" +
                        "<head>" +
                        "<style>" +
                        "* { font-size: 18px; }" +
                        "h1 { font-size: 32px; }" +
                        "a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;\n" +
                        "display: inline-flex;\n" +
                        "justify-content: center;\n" +
                        "align-items: center; color: #fff !important; }" +
                        "form { width: 100%; max-width: 300px; }" +
                        "label { display: block; }" +
                        "input, select { margin: 12px 0 20px 0; width: 100%; }" +
                        "</style>" +
                        "</head>" +
                        "<body>");
                out.println("<h1>Your Character:</h1>");
                out.println("<p>Name: " + name + "</p>");
                out.println("<p>Race: " + race + "</p>");
                out.println("<p>Weapon: " + weapon + "</p>");
                out.println("<p>Birthsign: " + birthsign + "</p>");
                out.println("<br/>");
                out.println("<a href=\"create-a-character\">Create a Character</a>\n" +
                        "<a href=\"characters-list\">View Characters</a> " +
                        "<a href=\"\\FinalProject_war_exploded\">Back Home</a>");
                out.println("</body></html>");
            } else {
                out.println("<html><head><style>a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;" +
                        "display: inline-flex;" +
                        "justify-content: center; " +
                        "align-items: center; color: #fff !important; }</style>" +
                        "</head><body><p>Something went wrong, create your character again. Be sure to fill the entire form</p><a href=\"create-a-character\">Create Character</a></body></html>");
            }
        } catch (JDBCException e) {
            out.println("<html><head><style>a { text-decoration: none; width: 200px; height: 42px; border-radius: 22px; background-color: #323232;" +
                    "display: inline-flex;" +
                    "justify-content: center; " +
                    "align-items: center; color: #fff !important; }</style>" +
                    "</head><body><p>Something went wrong, create your character again. Be sure to fill the entire form</p><a href=\"create-a-character\">Create Character</a></body></html>");
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }

    public void destroy() {
    }
}